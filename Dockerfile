ARG CLUSTER_APPLICATIONS_IMAGE_VERSION=v1.9.0
ARG KUSTOMIZE_IMAGE_VERSION=v5.0.0

FROM us.gcr.io/k8s-artifacts-prod/kustomize/kustomize:${KUSTOMIZE_IMAGE_VERSION} AS kustomize

FROM registry.gitlab.com/gitlab-org/cluster-integration/cluster-applications:${CLUSTER_APPLICATIONS_IMAGE_VERSION}

COPY --from=kustomize /app/kustomize /usr/local/bin/kustomize