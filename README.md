# Encrypt secrets

```bash
sops --encrypt --in-place <file>
```
